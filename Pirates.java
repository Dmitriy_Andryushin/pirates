/**
 * Программа рассчитывает: сколько драгоценных камней необходимо для того, чтобы всем пиратам досталось равное количество камней
 *
 *@author Andryushin Dmitriy 16IT18k
 */
public class Pirates {
    public static void main(String[] args) {
        for (double i = 300; i < 100000; i++) {
            if ((i%2)==1 & (i%3)==1 & (i%4)==1 & (i%6)==1 & (i%7)==0) {
                System.out.println(i);
            }
        }
    }
}
